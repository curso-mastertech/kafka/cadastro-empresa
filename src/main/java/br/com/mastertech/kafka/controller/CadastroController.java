package br.com.mastertech.kafka.controller;

import br.com.mastertech.kafka.service.RegistroEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cadastro")
public class CadastroController {

    @Autowired
    private RegistroEmpresaService registroService;

    @GetMapping("/{cnpj}")
    private void registrarEmpresa(@PathVariable(value = "cnpj") String cnpj) {
        registroService.registraEmpresa(cnpj);
    }
}
