package br.com.mastertech.kafka.service;

import br.com.mastertech.kafka.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class RegistroEmpresaService {

    public static final String TOPICO_REGISTRO_CNPJ = "pedro-biro-1";

    @Autowired
    private KafkaTemplate<String, Empresa> sender;

    public void registraEmpresa(String cnpj) {
        sender.send(TOPICO_REGISTRO_CNPJ, "1", new Empresa(cnpj));
    }
}
